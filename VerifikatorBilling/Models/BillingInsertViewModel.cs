﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VerifikatorBilling.Models
{
    public class BillingInsertViewModel
    {
        public bool audit { get; set; }
        public bool adaobat { get; set; }

        public string nobukti { get; set; }
        public string noreg { get; set; }
        public string section { get; set; }
        public int nomor { get; set; }
        public string dokter { get; set; }
        public string dokternama { get; set; }
        public decimal jumlah { get; set; }
        public DateTime tanggal { get; set; }
        public DateTime? tanggaldirujukkirim { get; set; }
        public DateTime? tanggaldirujukditerima { get; set; }
        public TimeSpan jam { get; set; }
        public List<BillingInsertDetailViewModel> detail { get; set; }

        public string kelas { get; set; }
        public string jenisanatesi { get; set; }
        public bool cito { get; set; }
        public bool dirujuk { get; set; }
        public string dirujukventor { get; set; }
        public string alasandirujuk { get; set; }
        public string spesialis { get; set; }
        public string ruangok { get; set; }
        public string kategorioperasi { get; set; }
        public string jenisluka { get; set; }
        public string dokterpengirim { get; set; }
        public string opetatorpelaksasa { get; set; }
        public string durante { get; set; }
        public string dokterasisten { get; set; }
        public string dokterasisten2 { get; set; }
        public string dokteranak { get; set; }
        public string dokteranastesi { get; set; }
        public string dokteranalis { get; set; }
        public List<SelectListItemViewModel> asistenanastesi { get; set; }
        public List<SelectListItemViewModel> instrumen { get; set; }
        public List<SelectListItemViewModel> onloop { get; set; }
    }

    public class BillingInsertDetailViewModel
    {
        public int id { get; set; }
        public string jasaid { get; set; }
        public string jasanama { get; set; }
        public string section { get; set; }
        public string noreg { get; set; }
        public decimal qty { get; set; }
        public decimal tarif { get; set; }
        public decimal diskon { get; set; }
        public string dokter { get; set; }
        public string namadokter { get; set; }
        public List<BillingInsertDetailJasaViewModel> detail { get; set; }
    }

    public class BillingInsertDetailJasaViewModel
    {
        public string komponenid { get; set; }
        public string komponennama { get; set; }
        public decimal harga { get; set; }
        public decimal diskon { get; set; }
        public decimal diskonnilai { get; set; }
        public decimal jumlah { get; set; }
    }
}
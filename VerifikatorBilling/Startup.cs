﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VerifikatorBilling.Startup))]
namespace VerifikatorBilling
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

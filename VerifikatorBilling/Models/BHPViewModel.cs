﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class BHPViewModel
    {
        public string NoReg { get; set; }
        public int Nomor { get; set; }
        public string SectionID { get; set; }
        public string NoBukti { get; set; }
        public string DokterID { get; set; }
        public string DokterNama { get; set; }
        public DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public bool Ditagihkan { get; set; }
        public string PaketBHP { get; set; }
        public string NamaPaket { get; set; }
        public decimal JumlahTransaksi { get; set; }
        public string JumlahTransaksi_View { get; set; }

        public ListDetail<BHPDetailViewModel> Detail_List { get; set; }
    }

    public class BHPDetailViewModel
    {
        public int Barang_Id { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public double Qty { get; set; }
        public double Stok { get; set; }
        public decimal HargaSatuan { get; set; }
        public string HargaSatuan_View { get; set; }
        public double Disc_Persen { get; set; }
        public string Jumlah_View { get; set; }
    }
}
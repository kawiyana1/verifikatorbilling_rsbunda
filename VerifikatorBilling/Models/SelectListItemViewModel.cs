﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class SelectListItemViewModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public string Data1 { get; set; }
        public bool Check1 { get; set; }
    }
}
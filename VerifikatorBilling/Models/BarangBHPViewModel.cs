﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class BarangBHPViewModel
    {
        public string Kode_Barang { get; set; }
        public string Satuan_Stok { get; set; }
        public bool Aktif { get; set; }
        public string Kelompok { get; set; }
        public string SectionID { get; set; }
        public string Nama_Kategori { get; set; }
        public double Qty_Stok { get; set; }
        public Nullable<bool> BarangLokasiNew_Aktif { get; set; }
        public int Barang_ID { get; set; }
        public string Nama_Sub_Kategori { get; set; }
        public Nullable<decimal> Harga_Jual { get; set; }
        public string Harga_Jual_View { get; set; }

        public string NamaBarang { get; set; }
    }
}
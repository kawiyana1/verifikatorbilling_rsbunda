﻿using iHos.MVC.Property;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class ResepViewModel
    {
        public int Nomor { get; set; }
        public string NoResep { get; set; }
        public string NoRegistrasi { get; set; }
        public System.DateTime Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public System.DateTime Jam { get; set; }
        public string Jam_View { get; set; }
        public string SectionID { get; set; }
        [Required]
        public string Farmasi_SectionID { get; set; }
        public string NamaFarmasi { get; set; }
        [Required]
        public string DokterID { get; set; }
        public string Dokter { get; set; }
        public decimal Jumlah { get; set; }
        public string Jumlah_View { get; set; }
        public bool Cyto { get; set; }
        public bool Realisasi { get; set; }
        public bool Batal { get; set; }
        public Nullable<int> NoAntri { get; set; }
        public string Keterangan { get; set; }
        public Nullable<bool> Puyer { get; set; }
        public Nullable<int> QtyPuyer { get; set; }
        public string SatuanPuyer { get; set; }
        public Nullable<double> BeratBadan { get; set; }
        public decimal KomisiDokter { get; set; }
        public string NoBukti { get; set; }
        public short User_ID { get; set; }
        public string JenisKerjasamaID { get; set; }
        public string CompanyID { get; set; }
        public string NRM { get; set; }
        public string NoKartu { get; set; }
        public string KelasID { get; set; }
        public bool KTP { get; set; }
        public string KerjasamaID { get; set; }
        public string PerusahaanID { get; set; }
        public bool RawatInap { get; set; }
        public Nullable<bool> Paket { get; set; }
        public string KodePaket { get; set; }
        public string NamaPaket { get; set; }
        public Nullable<bool> AmprahanRutin { get; set; }
        public Nullable<bool> IncludeJasa { get; set; }
        public string UserNameInput { get; set; }
        public Nullable<int> CustomerKerjasamaID { get; set; }
        public Nullable<bool> RasioObat_ada { get; set; }
        public Nullable<bool> Rasio_KelebihanDibayarPasien { get; set; }
        public Nullable<bool> RasioUmum_Alert { get; set; }
        public Nullable<bool> RasioUmum_Blok { get; set; }
        public Nullable<bool> RasioSpesialis_Alert { get; set; }
        public Nullable<bool> RasioSPesialis_Blok { get; set; }
        public Nullable<bool> RasioSub_Alert { get; set; }
        public Nullable<bool> RasioSub_Blok { get; set; }
        public Nullable<decimal> RasioUmum_nilai { get; set; }
        public Nullable<decimal> RasioSpesialis_Nilai { get; set; }
        public Nullable<decimal> RasioSub_Nilai { get; set; }
        public Nullable<bool> SedangProses { get; set; }
        public string SedangProsesPC { get; set; }
        public string AlasanBatal { get; set; }
        public Nullable<int> UserID_Batal { get; set; }
        public Nullable<bool> ObatPulang { get; set; }
        public Nullable<bool> Pending { get; set; }
        public string AlasanPending { get; set; }
        public Nullable<int> UserID_Pending { get; set; }
        public bool ObatRacik { get; set; }
        public double? QtyRacik { get; set; }

        public ListDetail<ResepDetailViewModel> Detail_List { get; set; }

        public string JedisResep { get; set; }
        public string SectionName { get; set; }
    }

    public class ResepDetailViewModel
    {
        public string NoResep { get; set; }
        public int Barang_ID { get; set; }
        public string Kode_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public string Satuan { get; set; }
        public string AturanPakai { get; set; }
        public Nullable<double> Dosis { get; set; }
        public double Qty { get; set; }
        public decimal Harga { get; set; }
        public string Harga_View { get; set; }
        public Nullable<double> Jumlah { get; set; }
        public string Jumlah_View { get; set; }
        public double Stok { get; set; }
        public bool Racik { get; set; }
    }
}
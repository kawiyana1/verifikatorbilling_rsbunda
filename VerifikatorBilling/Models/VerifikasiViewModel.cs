﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class VerifikasiViewModel
    {
        public string NoBukti { get; set; }
        public string NoReg { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        [DataType(DataType.Date)]
        public System.DateTime Tanggal { get; set; }
        [DataType(DataType.Time)]
        public System.DateTime Jam { get; set; }
        public Nullable<short> UserID { get; set; }
        public string UserIDWeb { get; set; }
        public string Catatan { get; set; }
        public string JenisKerjasama { get; set; }
        public string Nama_Customer { get; set; }
        public List<VerifikasiDetailViewModel> detail { get; set; }
    }

    public class VerifikasiDetailViewModel
    {
        public string NoBukti { get; set; }
        public string NoBuktiTransaksi { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public string NoReg { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string DokterID { get; set; }
        public string NamaDOkter { get; set; }
        public decimal NilaiPendapatan { get; set; }
        public string NilaiPendapatan_View { get; set; }
        public bool Transaksi_Dikunci { get; set; }
        public bool Ditagihkan { get; set; }
    }
}
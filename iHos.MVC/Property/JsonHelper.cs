﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace iHos.MVC.Property
{
    public static class JsonHelper
    {
        public static string JsonMsgError(ViewDataDictionary ViewData)
        {
            var rst = new ResultStatus()
            {
                IsSuccess = false,
                Message = VddToString(ViewData),
                Count = 0,
                Status = "danger"
            };
            return new JavaScriptSerializer().Serialize(rst);
        }

        public static string JsonMsgError(Exception ex)
        {
            if (ex.InnerException != null) { ex = ex.InnerException; }
            if (ex.InnerException != null) { ex = ex.InnerException; }
            var rst = new ResultStatus()
            {
                IsSuccess = false,
                Message = ex.Message,
                Count = 0,
                Status = "danger"
            };
            return new JavaScriptSerializer().Serialize(rst);
        }

        public static string JsonMsgError(string text)
        {
            var rst = new ResultStatus()
            {
                IsSuccess = false,
                Message = text,
                Count = 0,
                Status = "danger"
            };
            return new JavaScriptSerializer().Serialize(rst);
        }

        public static string JsonMsg(string message, bool isSuccess = false, int count = 0)
        {
            var rst = new ResultStatus()
            {
                IsSuccess = isSuccess,
                Message = message,
                Count = count,
                Status = isSuccess ? "success": "danger"
            };
            return new JavaScriptSerializer().Serialize(rst);
        }

        private static ResultStatus JsonMsgText(object resultss, int target, string message, object data = null)
        {
            var typ = resultss.GetType();
            var _cnt = (int)typ.GetProperty("Count").GetValue(resultss);
            var _suc = (bool)typ.GetProperty("IsSuccess").GetValue(resultss);
            string _msg;
            string _sts;
            if (_suc)
            {
                if (target != -1)
                    _suc = _suc && _cnt == target;
                _msg = _suc ? $"Data berhasil {message}" : $"Data yang {message} {_cnt}";
                _sts = _suc ? "success" : "warning";
            }
            else
            {
                _msg = (string)typ.GetProperty("Message").GetValue(resultss);
                _sts = "danger";
            }
            return new ResultStatus()
            {
                IsSuccess = _suc,
                Message = _msg,
                Count = _cnt,
                Status = _sts,
                Data = data
            };
        }

        public static string JsonMsgCreate(object resultss, object data,int target = 1)
        {
            return new JavaScriptSerializer().Serialize(JsonMsgText(resultss, target, "disimpan", data));
        }

        public static string JsonMsgCreate(object resultss, int target = 1)
        {
            return new JavaScriptSerializer().Serialize(JsonMsgText(resultss, target, "disimpan"));
        }

        public static string JsonMsgEdit(object resultss, int target = 1)
        {
            return new JavaScriptSerializer().Serialize(JsonMsgText(resultss, target, "diubah"));
        }

        public static string JsonMsgDelete(object resultss, int target = 1)
        {
            return new JavaScriptSerializer().Serialize(JsonMsgText(resultss, target, "dihapus"));
        }

        public static string JsonMsgCustom(object resultss, string message, int target = 1)
        {
            return new JavaScriptSerializer().Serialize(JsonMsgText(resultss, target, message));
        }

        public static string VddToString(ViewDataDictionary ViewData)
        {
            string result = "";
            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors) { result += error.ErrorMessage + "|"; }
            }
            return result.Remove(result.LastIndexOf("|"));
        }
    }
}

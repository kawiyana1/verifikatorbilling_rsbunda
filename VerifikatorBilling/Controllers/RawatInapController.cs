﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using VerifikatorBilling.Entities;
using VerifikatorBilling.Models;

namespace VerifikatorBilling.Controllers
{
    [Authorize(Roles = "VerifikatorBilling")]
    public class RawatInapController : Controller
    {
        #region ===== I N D E X
        public ActionResult Index()
        {
            Response.Cookies["TipePelayanan"].Value = "RawatInap";
            Response.Cookies["TipePelayanan"].Expires = DateTime.Now.AddDays(1);
            return View();
        }
        #endregion

        #region ===== T A B L E

        [HttpPost]
        public string List(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Verif_GetListRI> proses = s.Verif_GetListRI;
                    if (filter[24] != "True")
                    {
                        if (!string.IsNullOrEmpty(filter[22]))
                        {
                            proses = proses.Where("TglReg >= @0", DateTime.Parse(filter[22]).AddDays(-1));
                        }
                        if (!string.IsNullOrEmpty(filter[23]))
                        {
                            proses = proses.Where("TglReg <= @0", DateTime.Parse(filter[23]));
                        }

                    }
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Verif_GetListRI.NoReg)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Verif_GetListRI.NRM)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Verif_GetListRI.NamaPasien)}.Contains(@0)", filter[3]);
                    if (!string.IsNullOrEmpty(filter[4])) proses = proses.Where($"{nameof(Verif_GetListRI.ALamat)}.Contains(@0)", filter[4]);

                    if (!string.IsNullOrEmpty(filter[25]))
                    {
                        if (filter[25] != "ALL")
                        {
                            proses = proses.Where($"{nameof(Verif_GetListRI.SectionID)}.Contains(@0)", filter[25]);
                        }
                    }

                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<ListRawatInapViewModel>(x));
                    foreach (var x in m)
                    {
                        x.TglReg_View = x.TglReg.ToString("dd/MM/yyyy");
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}
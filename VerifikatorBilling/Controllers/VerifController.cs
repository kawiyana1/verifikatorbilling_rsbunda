﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VerifikatorBilling.Entities;
using VerifikatorBilling.Models;

namespace VerifikatorBilling.Controllers
{
    [Authorize(Roles = "VerifikatorBilling")]
    public class VerifController : Controller
    {
        // GET: Verif
        public ActionResult Index(string id)
        {
            var model = new VerifikasiViewModel();
            using (var s = new SIMEntities())
            {
                var m = s.VW_Registrasi.FirstOrDefault(x => x.NoReg == id);
                var v = s.SIMtrVerifikasi.Where(x => x.NoReg == id).FirstOrDefault();
                if (v != null)
                {
                    model = IConverter.Cast<VerifikasiViewModel>(m);
                    model.NoBukti = v.NoBukti;
                    model.Tanggal = v.Tanggal;
                    model.Tanggal = v.Tanggal;
                    model.Jam = v.Jam;
                    model.Catatan = v.Catatan;


                    model.JenisKerjasama = m.JenisKerjasama;
                    model.Nama_Customer = (m.Nama_Customer == null) ? "-" : m.Nama_Customer;
                }
                else
                {
                    model = IConverter.Cast<VerifikasiViewModel>(m);
                    model.NoBukti = null;
                    model.Tanggal = DateTime.Today;
                    model.Jam = DateTime.Now;
                    model.JenisKerjasama = m.JenisKerjasama;
                    model.Nama_Customer = (m.Nama_Customer == null) ? "-" : m.Nama_Customer;
                }
                
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        #region ===== B I L L I N G
        [HttpPost]
        [ActionName("Billing")]
        public string Billing_Post()
        {
            try
            {
                var item = new VerifikasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var verifdata = s.SIMtrVerifikasi.Where(x => x.NoBukti == item.NoBukti).FirstOrDefault();
                                if (verifdata !=  null)
                                {
                                    verifdata.Catatan = item.Catatan;
                                    verifdata.Tanggal = item.Tanggal;
                                    verifdata.Jam = item.Jam;
                                    verifdata.UserIDWeb = User.Identity.GetUserId();

                                    var SIMtrVerifikasiDetail = s.SIMtrVerifikasiDetail.Where(x => x.NoBuktiTransaksi == verifdata.NoBukti);
                                    if (SIMtrVerifikasiDetail != null)
                                    {
                                        s.SIMtrVerifikasiDetail.RemoveRange(SIMtrVerifikasiDetail);
                                    }
                                }
                                else
                                {
                                    var m = new SIMtrVerifikasi()
                                    {
                                        Catatan = item.Catatan,
                                        Jam = item.Jam,
                                        NoBukti = item.NoBukti,
                                        NoReg = item.NoReg,
                                        Tanggal = item.Tanggal,
                                        UserIDWeb = User.Identity.GetUserId()
                                    };
                                    s.SIMtrVerifikasi.Add(m);
                                }
                                
                                var r = s.SaveChanges();

                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrVerifikasiDetail()
                                    {
                                        NilaiPendapatan = x.NilaiPendapatan_View.ToDecimal(),
                                        NoBukti = x.NoBukti,
                                        NoReg = x.NoReg,
                                        NoBuktiTransaksi = item.NoBukti,
                                        SectionID = x.SectionID,
                                        Transaksi_Dikunci = x.Transaksi_Dikunci
                                    };
                                    s.SIMtrVerifikasiDetail.Add(i);
                                }
                                r = s.SaveChanges();

                                foreach (var x in item.detail)
                                {
                                    var simtrrj = s.SIMtrRJ.FirstOrDefault(z => z.NoBukti == x.NoBukti);
                                    simtrrj.Audit = x.Transaksi_Dikunci;
                                }
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrVerifikasi Create {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                {
                    if (!ModelState.IsValid)
                    {

                        foreach (ModelState modelState in ViewData.ModelState.Values)
                        {
                            foreach (ModelError error in modelState.Errors)
                            {
                                throw new Exception(ViewData.ModelState.Values + " " + error.ErrorMessage);
                            }
                        }

                    }
                    return JsonHelper.JsonMsgError(ViewData);
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== O B A T
        [HttpPost]
        [ActionName("Obat")]
        public string Obat_Post()
        {
            try
            {
                var item = new VerifikasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var verifdata = s.SIMtrVerifikasi.Where(x => x.NoBukti == item.NoBukti).FirstOrDefault();
                                if (verifdata != null)
                                {
                                    verifdata.Catatan = item.Catatan;
                                    verifdata.Tanggal = item.Tanggal;
                                    verifdata.Jam = item.Jam;
                                    verifdata.UserIDWeb = User.Identity.GetUserId();

                                    var SIMtrVerifikasiDetail = s.SIMtrVerifikasiDetail.Where(x => x.NoBuktiTransaksi == verifdata.NoBukti);
                                    if (SIMtrVerifikasiDetail != null)
                                    {
                                        s.SIMtrVerifikasiDetail.RemoveRange(SIMtrVerifikasiDetail);
                                    }
                                }
                                else
                                {
                                    var m = new SIMtrVerifikasi()
                                    {
                                        Catatan = item.Catatan,
                                        Jam = item.Jam,
                                        NoBukti = item.NoBukti,
                                        NoReg = item.NoReg,
                                        Tanggal = item.Tanggal,
                                        UserIDWeb = User.Identity.GetUserId()
                                    };
                                    s.SIMtrVerifikasi.Add(m);
                                }

                                var r = s.SaveChanges();

                                foreach (var x in item.detail)
                                {
                                    var i = new SIMtrVerifikasiDetail()
                                    {
                                        NilaiPendapatan = x.NilaiPendapatan_View.ToDecimal(),
                                        NoBukti = x.NoBukti,
                                        NoReg = x.NoReg,
                                        NoBuktiTransaksi = item.NoBukti,
                                        SectionID = x.SectionID,
                                        Transaksi_Dikunci = x.Transaksi_Dikunci
                                    };
                                    s.SIMtrVerifikasiDetail.Add(i);
                                }
                                r = s.SaveChanges();

                                foreach (var x in item.detail)
                                {
                                    var billfarmasi = s.BILLFarmasi.FirstOrDefault(z => z.NoBukti == x.NoBukti);
                                    billfarmasi.ProsesAudit = x.Transaksi_Dikunci;
                                }
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrVerifikasi Create {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== B H P
        [HttpPost]
        [ActionName("BHP")]
        public string BHP_Post()
        {
            try
            {
                var item = new VerifikasiViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var verifdata = s.SIMtrVerifikasi.Where(x => x.NoBukti == item.NoBukti).FirstOrDefault();
                                if (verifdata != null)
                                {
                                    verifdata.Catatan = item.Catatan;
                                    verifdata.Tanggal = item.Tanggal;
                                    verifdata.Jam = item.Jam;
                                    verifdata.UserIDWeb = User.Identity.GetUserId();

                                    var SIMtrVerifikasiDetail = s.SIMtrVerifikasiDetail.Where(x => x.NoBuktiTransaksi == verifdata.NoBukti);
                                    if (SIMtrVerifikasiDetail != null)
                                    {
                                        s.SIMtrVerifikasiDetail.RemoveRange(SIMtrVerifikasiDetail);
                                    }
                                }
                                else
                                {
                                    var m = new SIMtrVerifikasi()
                                    {
                                        Catatan = item.Catatan,
                                        Jam = item.Jam,
                                        NoBukti = item.NoBukti,
                                        NoReg = item.NoReg,
                                        Tanggal = item.Tanggal,
                                        UserIDWeb = User.Identity.GetUserId()
                                    };
                                    s.SIMtrVerifikasi.Add(m);
                                }

                                var r = s.SaveChanges();

                                if (item.detail != null) { 
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrVerifikasiDetail()
                                        {
                                            NilaiPendapatan = x.NilaiPendapatan_View.ToDecimal(),
                                            NoBukti = x.NoBukti,
                                            NoReg = x.NoReg,
                                            NoBuktiTransaksi = item.NoBukti,
                                            SectionID = x.SectionID,
                                            Transaksi_Dikunci = x.Transaksi_Dikunci
                                        };
                                        s.SIMtrVerifikasiDetail.Add(i);
                                    }
                                    foreach (var x in item.detail)
                                    {
                                        var simtrpop = s.SIMtrPOP.FirstOrDefault(z => z.NoBuktiPOP == x.NoBukti);
                                        simtrpop.Audit = x.Transaksi_Dikunci;
                                    }
                                }
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"SIMtrVerifikasi Create {item.NoBukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== A U T O I D

        public string AutoId()
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    var m = s.AutoNumber_Verif_Billing().FirstOrDefault();
                    if (m == null) return JsonHelper.JsonMsgError("Data tidak ditemukan");
                    result = new ResultSS(m) { IsSuccess = true };
                }
                return JsonConvert.SerializeObject(result);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        [HttpGet]
        public ActionResult DetailBiaya(string noreg)
        {
            ViewBag.NoReg = noreg;
            return PartialView();
        }

        [HttpPost]
        public string ListDetailBiaya(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<GetDetailRincianBiaya_Result> proses = s.GetDetailRincianBiaya(filter[11], (int?)null);
                    if (!string.IsNullOrEmpty(filter[12]))
                        proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.KategoriBiaya)}=@0", filter[12]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.NoBukti)}.Contains(@0)", filter[0]);
                    if (IFilter.F_DateTime(filter[1]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Tanggal)}=@0", IFilter.F_DateTime(filter[1]));
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.JenisBiaya)}.Contains(@0)", filter[2]);
                    if (IFilter.F_Decimal(filter[3]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Qty)}=@0", (double)IFilter.F_Decimal(filter[3]));
                    if (IFilter.F_Decimal(filter[4]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Nilai)}=@0", IFilter.F_Decimal(filter[4]));
                    if (IFilter.F_Decimal(filter[5]) != null) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.Disc)}=@0", (double)IFilter.F_Decimal(filter[5]));
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.SectionName)}.Contains(@0)", filter[6]);
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(GetDetailRincianBiaya_Result.DokterName)}.Contains(@0)", filter[7]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<DetailBiayaViewModel>(x));
                    var totaldiskon = proses.Sum(x => x.Disc);
                    var totalqty = proses.Sum(x => x.Qty);
                    foreach (var x in m)
                    {
                        x.TotalDisk = totaldiskon ?? 0;
                        x.TotalQty = totalqty ?? 0;
                        x.NilaiRumus = (x.Nilai * (decimal)x.Qty) + x.HExt;
                        x.Nilai_View = ((decimal)(x.Nilai ?? 0)).ToMoney();
                        x.Tanggal_View = x.Tanggal == null ? "" : x.Tanggal.Value.ToString("dd/MM/yyyy");
                    }
                    if (m.Count() > 0)
                    {
                        m[0].GrandTotal_View = ((decimal)m.Sum(x => x.NilaiRumus ?? 0)).ToMoney();
                        m[0].GrandTotal = (decimal)m.Sum(x => x.NilaiRumus ?? 0);
                    }
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        public string getTotalDetailBiaya(string noreg)
        {
            try
            {
                decimal total = 0;
                using (var s = new SIMEntities())
                {
                    var l = s.GetDetailRincianBiaya(noreg, (int?)null).ToList();

                    foreach (var x in l)
                    {
                        total += (decimal)x.Nilai * (decimal)x.Qty;
                    }
                }
                return JsonConvert.SerializeObject(new
                {
                    IsSuccess = true,
                    Data = total.ToMoney(),
                    Message = "Success.."
                });
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpGet]
        [ActionName("UpdateKelasRI")]
        public ActionResult UpdateKelasRI_Get(string noreg)
        {
            UpdateKelasViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    if (m == null) return HttpNotFound();
                    item = new UpdateKelasViewModel()
                    {
                        Reg_KdKelasPertanggungan = m.KdKelasPertanggungan,
                        Reg_KdKelasAsal = m.KdKelasAsal,
                        Reg_KdKelas = m.KdKelas,
                        TitipKelas = m.TitipKelas == null ? false : m.TitipKelas
                    };
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("UpdateKelasRI")]
        public string UpdateKelasRI_Post()
        {
            try
            {
                var item = new UpdateKelasViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        var kelas = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.NoReg);
                        if (kelas != null)
                        {
                            kelas.KdKelasPertanggungan = item.Reg_KdKelasPertanggungan;
                            kelas.KdKelasAsal = item.Reg_KdKelasAsal;
                            kelas.KdKelas = item.Reg_KdKelas;
                            kelas.TitipKelas = item.TitipKelas;
                        }
                        s.SaveChanges();
                        result = new ResultSS(1);
                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"Uptasi Pasien {item.NoReg} {item.Nomor}"
                        };
                        UserActivity.InsertUserActivity(userActivity);
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { if (ex.InnerException != null) ex = ex.InnerException; return JsonHelper.JsonMsgError(ex); }
        }
    }
}
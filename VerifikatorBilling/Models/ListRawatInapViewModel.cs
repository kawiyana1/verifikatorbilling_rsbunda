﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class ListRawatInapViewModel
    {
        public string NoReg { get; set; }
        public System.DateTime TglReg { get; set; }
        public string TglReg_View { get; set; }
        public string NoKamar { get; set; }
        public string NoBed { get; set; }
        public string NRM { get; set; }
        public string NamaPasien { get; set; }
        public string JenisKelamin { get; set; }
        public string Perusahaan { get; set; }
        public string NoKartu { get; set; }
        public string ALamat { get; set; }
        public string NamaKelas { get; set; }
        public string JenisKerjasama { get; set; }
        public string DokterID { get; set; }
        public string SectionID { get; set; }
        public string SectionName { get; set; }
        public string StatusBayar { get; set; }
    }
}
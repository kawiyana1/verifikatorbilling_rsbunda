﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VerifikatorBilling.Entities;

namespace VerifikatorBilling.Models
{
    public class StaticModel
    {
        public static string DbEntityValidationExceptionToString(DbEntityValidationException ex)
        {
            var msgs = new List<string>();
            foreach (var eve in ex.EntityValidationErrors)
            {
                var msg = $"Entity of type \"{eve.Entry.Entity.GetType().Name}\" in state \"{eve.Entry.State}\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors) { msg += $"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\""; }
                msgs.Add(msg);
            }
            return string.Join("\n", msgs.ToArray());
        }
        public static List<SelectListItem> ListKelas
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmKelas.Where(x => x.Active == true).ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.KelasID,
                            Text = item.NamaKelas
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionRJ
        {
            get
            {
                var r = new List<SelectListItem>();
                r.Add(new SelectListItem()
                {
                    Value = "ALL",
                    Text = "ALL"
                });

                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "RJ" || x.TipePelayanan == "PENUNJANG2").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID,
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListSectionRI
        {
            get
            {
                var r = new List<SelectListItem>();
                r.Add(new SelectListItem()
                {
                    Value = "ALL",
                    Text = "ALL"
                });

                using (var s = new SIMEntities())
                {
                    var m = s.SIMmSection.Where(x => x.TipePelayanan == "RI").ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.SectionID,
                            Text = item.SectionName
                        });
                    }
                }
                return r;
            }
        }

        public static List<SelectListItem> ListJenisAnatesi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.mJenisAnestesi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Deskripsi,
                        Value = x.IDAnastesi
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListSpesialisasi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.SIMmSpesialisasi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.SpesialisName,
                        Value = x.SpesialisID
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListRuangOK
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.SIMmRuangOK.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.RuangOK,
                        Value = x.RuangOKID
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListKategoriOperasi
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.SIMmKategoriOperasi.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.KategoriName,
                        Value = x.KategoriID.ToString()
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListJenisLuka
        {
            get
            {
                List<SelectListItem> r;
                using (var s = new SIMEntities())
                {
                    r = s.SIMmJenisLuka.ToList().ConvertAll(x => new SelectListItem()
                    {
                        Text = x.Deskripsi,
                        Value = x.KodeLuka
                    });
                }
                return r;
            }
        }
        public static List<SelectListItem> ListAlasanDirujuk
        {
            get
            {
                var r = new List<SelectListItem>();
                using (var s = new SIMEntities())
                {
                    var m = s.SIMmAlasanDirujuk.ToList();
                    foreach (var item in m)
                    {
                        r.Add(new SelectListItem()
                        {
                            Value = item.AlasanDirujukID,
                            Text = item.AlasanDirujuk
                        });
                    }
                }
                return r;
            }
        }
    }
}
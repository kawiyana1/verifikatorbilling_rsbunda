﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class DetailBiayaViewModel
    {
        public string NoBukti { get; set; }
        public Nullable<System.DateTime> Tanggal { get; set; }
        public string Tanggal_View { get; set; }
        public Nullable<System.DateTime> Jam { get; set; }
        public string GroupBiaya { get; set; }
        public string KategoriBiaya { get; set; }
        public string KodeJenisBiaya { get; set; }
        public string JenisBiaya { get; set; }
        public Nullable<double> Qty { get; set; }
        public Nullable<decimal> Nilai { get; set; }
        public string Nilai_View { get; set; }
        public Nullable<decimal> KelebihanPlafon { get; set; }
        public string SectionName { get; set; }
        public string DokterName { get; set; }
        public string UserName { get; set; }
        public string DOkterID { get; set; }
        public string GroupJasa { get; set; }
        public Nullable<int> GroupJasaID { get; set; }
        public string Keterangan { get; set; }
        public Nullable<double> Disc { get; set; }
        public Nullable<decimal> HargaOrig { get; set; }
        public Nullable<decimal> HExt { get; set; }
        public decimal GrandTotal { get; set; }
        public double TotalQty { get; set; }
        public double TotalDisk { get; set; }
        public string GrandTotal_View { get; set; }
        public Nullable<decimal> NilaiRumus { get; set; }
    }
}
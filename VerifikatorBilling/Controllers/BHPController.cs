﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VerifikatorBilling.Entities;
using VerifikatorBilling.Models;
namespace VerifikatorBilling.Controllers
{
    [Authorize(Roles = "VerifikatorBilling")]
    public class BHPController : Controller
    {
        #region ==== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ===== E D I T

        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti)
        {
            BHPViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.Pelayanan_SIMtrPOPPaket.FirstOrDefault(x => x.NoBuktiPOP == nobukti);
                    if (m == null) return HttpNotFound();
                    item = new BHPViewModel()
                    {
                        NoBukti = m.NoBuktiPOP,
                        Tanggal = m.Tanggal,
                        Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam_View = m.Jam.ToString("HH:mm"),
                        Jam = m.Jam,
                        NamaPaket = m.NamaPaket,
                        JumlahTransaksi_View = m.JumlahTransaksi.ToMoney(),
                        Ditagihkan = m.Ditagihkan ?? false,
                        DokterID = m.DokterID,
                        JumlahTransaksi = m.JumlahTransaksi,
                        NoReg = m.NoReg,
                        SectionID = m.SectionID,
                        DokterNama = m.NamaDOkter,
                        PaketBHP = m.PaketObat
                    };
                    var d = s.SIMtrPOPPaket_Detail.Where(x => x.NoBuktiPOP == nobukti).ToList();
                    item.Detail_List = new ListDetail<BHPDetailViewModel>();
                    foreach (var x in d)
                    {
                        item.Detail_List.Add(false, new BHPDetailViewModel()
                        {
                            Barang_Id = x.Barang_Id,
                            Disc_Persen = x.Disc_Persen,
                            HargaSatuan = x.HargaSatuan,
                            HargaSatuan_View = x.HargaSatuan.ToMoney(),
                            KodeBarang = x.Kode_Barang,
                            NamaBarang = x.Nama_Barang,
                            Qty = x.Qty,
                            Satuan = x.Satuan_Stok,
                            Jumlah_View = ((decimal)(x.HargaTotal ?? 0)).ToMoney()
                        });
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }

        [HttpPost]
        [ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public string Edit_Post()
        {
            try
            {
                var item = new BHPViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    ResultSS result;
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                var user = User.Identity.GetUserId();
                                #region Validation
                                var model = s.SIMtrPOP.FirstOrDefault(x => x.NoBuktiPOP == item.NoBukti);
                                if (model == null) throw new Exception("Data Tidak ditemukan");

                                #endregion

                                #region Header
                                model.Ditagihkan = item.Ditagihkan;
                                #endregion

                                result = new ResultSS(s.SaveChanges());
                                dbContextTransaction.Commit();
                            }
                            catch (DbEntityValidationException ex) { dbContextTransaction.Rollback(); throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex)); }
                            catch (SqlException ex) { dbContextTransaction.Rollback(); throw new Exception(ex.Message); }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                if (ex.InnerException != null)
                                {
                                    if (ex.InnerException.Message == "Conflicting changes detected. This may happen when trying to insert multiple entities with the same key.")
                                    {
                                        throw new Exception("Detail tidak boleh sama");
                                    }
                                }
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                    return JsonHelper.JsonMsgEdit(result, -1);
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ===== T A B L E - B A R A N G
        [HttpPost]
        public string ListBarang(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_GetListObat_Result> proses = s.Pelayanan_GetListObat(filter[8], filter[10]);
                    if (!string.IsNullOrEmpty(filter[0])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Kode_Barang)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.NamaBarang)}.Contains(@0)", filter[1]);
                    if (!string.IsNullOrEmpty(filter[2])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Satuan_Stok)}.Contains(@0)", filter[2]);
                    if (!string.IsNullOrEmpty(filter[3])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Qty_Stok)}=@0", IFilter.F_Decimal(filter[3]));
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Sub_Kategori)}.Contains(@0)", filter[4]);
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Kategori)}.Contains(@0)", filter[5]);
                    proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Aktif)}=@0", true);
                    if (!string.IsNullOrEmpty(filter[6])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Nama_Kategori)}.Contains(@0)", filter[6].ToUpper() == "Y");
                    if (!string.IsNullOrEmpty(filter[7])) proses = proses.Where($"{nameof(Pelayanan_GetListObat_Result.Harga_Jual)}=@0", IFilter.F_Decimal(filter[7]));
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<BarangBHPViewModel>(x));
                    m.ForEach(x => x.Harga_Jual_View = (x.Harga_Jual ?? 0).ToMoney());
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ===== T A B L E - P A K E T - B H P

        [HttpPost]
        public string ListPaketBHP(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_GetPaketBHP_Result> proses = s.Pelayanan_GetPaketBHP(filter[3]).AsQueryable();
                    proses = proses.Where($"{nameof(Pelayanan_GetPaketBHP_Result.Kode)}.Contains(@0)", filter[0]);
                    proses = proses.Where($"{nameof(Pelayanan_GetPaketBHP_Result.NamaPaket)}.Contains(@0)", filter[1]);
                    proses = proses.Where($"{nameof(Pelayanan_GetPaketBHP_Result.Ditagihkan)}.Contains(@0)", filter[2]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<PaketBHPViewModel>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        [HttpPost]
        public string DetailPaketBHP(string section, string kode)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var d = s.Pelayanan_GetPaketBHPDetail(section).Where(x => x.Kode == kode).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = d.ConvertAll(x => new
                        {
                            Kode = x.Kode,
                            NamaPaket = x.NamaPaket,
                            SectionID = x.SectionID,
                            SectionName = x.SectionName,
                            Barang_ID = x.Barang_ID,
                            Kode_Barang = x.Kode_Barang,
                            Nama_Barang = x.Nama_Barang,
                            Nama_Kategori = x.Nama_Kategori,
                            Satuan_Stok = x.Satuan_Stok,
                            Harga_Jual = x.Harga_Jual,
                            Harga_Jual_View = (x.Harga_Jual ?? 0).ToMoney(),
                            Jumlah_View = ((x.Harga_Jual ?? 0) * (decimal)(x.Qty ?? 0)).ToMoney(),
                            Qty = x.Qty,
                            Ditagihkan = x.Ditagihkan,
                            Qty_Stok = x.Qty_Stok
                        })
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion

        #region ==== G E T  D A T A  B H P
        [HttpPost]
        public string GetDataBHP(string noreg, string status)
        {
            try
            {
                var item = new VerifikasiViewModel();
                using (var s = new SIMEntities())
                {
                    var detail = s.Verif_DataBHP(noreg).ToList();
                    if (!string.IsNullOrEmpty(status))
                    {
                        var audit = status == "y" ? true : false;
                        detail = detail.Where(x => x.NoReg == noreg && x.Audit == audit).ToList();
                    }

                    item.detail = new List<VerifikasiDetailViewModel>();
                    foreach (var x in detail)
                    {
                        var y = new VerifikasiDetailViewModel()
                        {
                            NilaiPendapatan = x.Nilai == null ? 0 : (decimal)x.Nilai,
                            NilaiPendapatan_View = x.Nilai == null ? "0" : x.Nilai.Value.ToMoney(),
                            NoBukti = x.NoBukti,
                            NoReg = x.NoReg,
                            SectionID = x.SectionID,
                            DokterID = x.DokterID,
                            NamaDOkter = x.NamaDokter,
                            SectionName = x.SectionName,
                            Tanggal = x.Tanggal,
                            Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy"),
                            Transaksi_Dikunci = x.Audit.Value,
                            Ditagihkan = x.Ditagihkan.Value,
                        };
                        item.detail.Add(y);
                    }
                }
                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string nobukti)
        {
            BHPViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.Pelayanan_SIMtrPOPPaket.FirstOrDefault(x => x.NoBuktiPOP == nobukti);
                    if (m == null) return HttpNotFound();
                    item = new BHPViewModel()
                    {
                        NoBukti = m.NoBuktiPOP,
                        Tanggal = m.Tanggal,
                        Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy"),
                        Jam_View = m.Jam.ToString("HH:mm"),
                        Jam = m.Jam,
                        NamaPaket = m.NamaPaket,
                        JumlahTransaksi_View = m.JumlahTransaksi.ToMoney(),
                        Ditagihkan = m.Ditagihkan ?? false,
                        DokterID = m.DokterID,
                        JumlahTransaksi = m.JumlahTransaksi,
                        NoReg = m.NoReg,
                        SectionID = m.SectionID,
                        DokterNama = m.NamaDOkter,
                        PaketBHP = m.PaketObat
                    };
                    var d = s.SIMtrPOPPaket_Detail.Where(x => x.NoBuktiPOP == nobukti).ToList();
                    item.Detail_List = new ListDetail<BHPDetailViewModel>();
                    foreach (var x in d)
                    {
                        item.Detail_List.Add(false, new BHPDetailViewModel()
                        {
                            Barang_Id = x.Barang_Id,
                            Disc_Persen = x.Disc_Persen,
                            HargaSatuan = x.HargaSatuan,
                            HargaSatuan_View = x.HargaSatuan.ToMoney(),
                            KodeBarang = x.Kode_Barang,
                            NamaBarang = x.Nama_Barang,
                            Qty = x.Qty,
                            Satuan = x.Satuan_Stok,
                            Jumlah_View = ((decimal)(x.HargaTotal ?? 0)).ToMoney()
                        });
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
    }
}
﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VerifikatorBilling.Entities;
using VerifikatorBilling.Models;

namespace VerifikatorBilling.Controllers
{
    [Authorize(Roles = "VerifikatorBilling")]
    public class BillingController : Controller
    {
        #region ==== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ==== G E T  D A T A  B I L L I N G
        [HttpPost]
        public string GetDataBilling(string noreg, string status)
        {
            try
            {
                var item = new VerifikasiViewModel();
                using (var s = new SIMEntities())
                {
                    var detail = s.Verif_DataBilling(noreg).ToList();
                    if (!string.IsNullOrEmpty(status))
                    {
                        var audit = status == "y" ? true : false;
                        detail = detail.Where(x => x.NoReg == noreg && x.Audit == audit).ToList();
                    } 

                    item.detail = new List<VerifikasiDetailViewModel>();
                    foreach (var x in detail)
                    {
                        var y = new VerifikasiDetailViewModel()
                        {
                            NilaiPendapatan = x.Nilai == null ? 0 : (decimal)x.Nilai,
                            NilaiPendapatan_View = x.Nilai == null ? "0" : x.Nilai.Value.ToMoney(),
                            NoBukti = x.NoBukti,
                            NoReg = x.NoReg,
                            SectionID = x.SectionID,
                            DokterID = x.DokterID,
                            NamaDOkter = x.NamaDokter,
                            SectionName = x.SectionName,
                            Tanggal = x.Tanggal,
                            Tanggal_View = x.Tanggal != null ? x.Tanggal.Value.ToString("dd/MM/yyyy") : "",
                            Transaksi_Dikunci = x.Audit ?? false,
                            Ditagihkan = false,
                            NoBuktiTransaksi = "",
                            
                        };
                        item.detail.Add(y);
                    }
                }
                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== E D I T  R J  R I
        [HttpGet]
        [ActionName("Edit")]
        public ActionResult Edit_Get(string nobukti, string sectionid)
        {
            Response.Cookies["SectionID"].Value = sectionid;
            Response.Cookies["SectionID"].Expires = DateTime.Now.AddDays(1);
            BillingInsertViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    dokter = m.DokterID,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    nobukti = m.NoBukti,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    detail = new List<BillingInsertDetailViewModel>()
                };

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }
            }
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public string Edit_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region CHECK HEADER
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                var nobukti = SIMtrRJ.NoBukti;
                                #endregion

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                #endregion

                                var userid = User.Identity.GetUserId();
                                SIMtrRJ.RegNo = item.noreg;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.MCU = false;
                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon,
                                        };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;
                                    }
                                }
                                
                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {item.nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== E D I T  O K
        [HttpGet]
        [ActionName("EditOK")]
        public ActionResult EditOK_Get(string nobukti, string sectionid)
        {
            Response.Cookies["SectionID"].Value = sectionid;
            Response.Cookies["SectionID"].Expires = DateTime.Now.AddDays(1);
            BillingInsertViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();

                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    kelas = m.KdKelas,
                    jenisanatesi = m.OK_JenisAnastesi,
                    spesialis = m.SpesialisasiID,
                    ruangok = m.RuangOK,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    kategorioperasi = m.OK_KategoriOperasiID.ToString(),
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;

                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter, Check1 = x.D });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter, Check1 = x.D });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter, Check1 = x.D });
                }

                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);

                if (spesialisasi != null) ViewBag.SpesialisasiName = spesialisasi.SpesialisName;
                if (ruangok != null) ViewBag.RuangOKName = ruangok.RuangOK;

                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditOK")]
        public string EditOK_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region CHECK
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                var nobukti = SIMtrRJ.NoBukti;

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var userid = User.Identity.GetUserId();
                                #endregion

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }

                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                var SIMtrOperasiOnLoop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiOnLoop != null)
                                {
                                    s.SIMtrOperasiOnLoop.RemoveRange(SIMtrOperasiOnLoop);
                                }
                                #endregion

                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.KdKelas = item.kelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.SectionAsalID = reg.SectionAsalID;
                                SIMtrRJ.OK_JenisAnastesi = item.jenisanatesi;
                                SIMtrRJ.Cyto = item.cito;
                                SIMtrRJ.SpesialisasiID = item.spesialis;
                                SIMtrRJ.RuangOK = item.ruangok;
                                SIMtrRJ.OK_KategoriOperasiID = string.IsNullOrEmpty(item.kategorioperasi) ? (byte?)null : byte.Parse(item.kategorioperasi);
                                SIMtrRJ.JenisLukaID = item.jenisluka;
                                SIMtrRJ.DokterPengirimID = item.dokterpengirim;
                                SIMtrRJ.OK_DokterBedahID = item.dokter;
                                SIMtrRJ.OK_DokterBedahID2 = item.opetatorpelaksasa;
                                SIMtrRJ.OK_DokterDuranteID = item.durante;
                                SIMtrRJ.OK_DOkterAssBedah = item.dokterasisten;
                                SIMtrRJ.OK_DOkterAssBedah2 = item.dokterasisten2;
                                SIMtrRJ.OK_DokterANak = item.dokteranak;
                                SIMtrRJ.OK_DokterAnasID = item.dokteranastesi;
                                SIMtrRJ.OK_PenanggungNama = simtrreg.PenanggungNama;
                                SIMtrRJ.OK_PenanggungAlamat = simtrreg.PenanggungAlamat;
                                SIMtrRJ.OK_PenanggungHub = simtrreg.PenanggungHubungan;
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = false;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon
                                        };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;
                                    }
                                }
                                
                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value, D = x.Check1 });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value, D = x.Check1 });
                                }
                                if (item.onloop == null) item.onloop = new List<SelectListItemViewModel>();
                                foreach (var x in item.onloop)
                                {
                                    s.SIMtrOperasiOnLoop.Add(new SIMtrOperasiOnLoop() { NoBukti = nobukti, DokterOnLoopID = x.Value, D = x.Check1 });
                                }
                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                //var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                //simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing OK Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== E D I T  V K
        [HttpGet]
        [ActionName("EditVK")]
        public ActionResult EditVK_Get(string nobukti, string sectionid)
        {
            Response.Cookies["SectionID"].Value = sectionid;
            Response.Cookies["SectionID"].Expires = DateTime.Now.AddDays(1);
            BillingInsertViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;

                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterpengirim);
                ViewBag.Pengirim = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditVK")]
        public string EditVK_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region Delete
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                var nobukti = SIMtrRJ.NoBukti;

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var userid = User.Identity.GetUserId();
                                #endregion


                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.KdKelas = item.kelas;
                                SIMtrRJ.OK_PenanggungNama = simtrreg.PenanggungNama;
                                SIMtrRJ.OK_PenanggungAlamat = simtrreg.PenanggungAlamat;
                                SIMtrRJ.OK_PenanggungHub = simtrreg.PenanggungHubungan;
                                SIMtrRJ.OK_DokterBedahID = item.dokter;
                                SIMtrRJ.OK_DokterBedahID2 = item.opetatorpelaksasa;
                                SIMtrRJ.OK_DokterDuranteID = item.durante;
                                SIMtrRJ.OK_DOkterAssBedah = item.dokterasisten;
                                SIMtrRJ.OK_DokterANak = item.dokteranak;
                                SIMtrRJ.OK_DokterAnasID = item.dokteranastesi;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.DokterPengirimID = item.dokterpengirim;
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = false;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon
                                        };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;
                                    }
                                }
                                
                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value });
                                }
                                //if (item.onloop == null) item.onloop = new List<SelectListItem>();
                                //foreach (var x in item.onloop)
                                //{
                                //    s.SIMtrOperasiOnLoop.Add(new SIMtrOperasiOnLoop() { NoBukti = m.NoBukti, DokterOnLoopID = x.Value });
                                //}
                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                simtrdataregpasien.SudahPeriksa = true;
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region  ==== E D I T  L A B
        [HttpGet]
        [ActionName("EditLAB")]
        public ActionResult EditLAB_Get(string nobukti, string sectionid)
        {
            Response.Cookies["SectionID"].Value = sectionid;
            Response.Cookies["SectionID"].Expires = DateTime.Now.AddDays(1);
            BillingInsertViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nobukti = m.NoBukti,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.DokterID,
                    dirujuk = m.Dirujuk ?? false,
                    dokteranalis = m.AnalisID,
                    tanggaldirujukditerima = m.TglTerima,
                    tanggaldirujukkirim = m.TglKirim,
                    dirujukventor = m.DirujukVendorID,
                    alasandirujuk = m.AlasanDirujukID,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                };

                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;

                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;
                ViewBag.Keterangan = m.Keterangan;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Dokter = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranalis);
                ViewBag.Radiografer = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dirujukventor);
                ViewBag.Vendor = dokter != null ? dokter.NamaDOkter : "";

                var alasandirujuk = s.SIMmAlasanDirujuk.FirstOrDefault(x => x.AlasanDirujukID == model.alasandirujuk);
                ViewBag.Alasan = alasandirujuk != null ? alasandirujuk.AlasanDirujuk : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditLAB")]
        public string EditLAB_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region CHECK
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                var nobukti = SIMtrRJ.NoBukti;

                                var reg = s.VW_DataPasienReg_Pelayanan.FirstOrDefault(x => x.NoReg == item.noreg && x.Nomor == item.nomor);
                                var simtrreg = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var userid = User.Identity.GetUserId();
                                #endregion


                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.SupplierPengirimID = reg.DokterID;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.AnalisID = item.dokteranalis;
                                SIMtrRJ.Dirujuk = item.dirujuk;
                                SIMtrRJ.DirujukVendorID = item.dirujuk ? item.dirujukventor : "";
                                SIMtrRJ.TglKirim = item.dirujuk ? item.tanggaldirujukkirim : (DateTime?)null;
                                SIMtrRJ.TglTerima = item.dirujuk ? item.tanggaldirujukditerima : (DateTime?)null;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.AlasanDirujukID = item.dirujuk ? item.alasandirujuk : string.Empty;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.MCU = false;
                                SIMtrRJ.UserIDWeb = userid;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon
                                        };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;
                                    }
                                }
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Edit {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== E D I T  R A D
        [HttpGet]
        [ActionName("EditRAD")]
        public ActionResult EditRAD_Get(string nobukti, string sectionid)
        {
            Response.Cookies["SectionID"].Value = sectionid;
            Response.Cookies["SectionID"].Expires = DateTime.Now.AddDays(1);
            BillingInsertViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.SupplierPengirimID,
                    dirujukventor = m.DirujukVendorID,
                    alasandirujuk = m.AlasanDirujukID,
                    dirujuk = m.Dirujuk ?? false,
                    dokteranalis = m.AnalisID,
                    dokter = m.DokterID,
                    tanggaldirujukditerima = m.TglTerima,
                    tanggaldirujukkirim = m.TglKirim,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                };
                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;

                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.SupplierPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;
                ViewBag.Keterangan = m.Keterangan;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Dokter = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranalis);
                ViewBag.Radiografer = dokter != null ? dokter.NamaDOkter : "";

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dirujukventor);
                ViewBag.Vendor = dokter != null ? dokter.NamaDOkter : "";

                var alasandirujuk = s.SIMmAlasanDirujuk.FirstOrDefault(x => x.AlasanDirujukID == model.alasandirujuk);
                ViewBag.Alasan = alasandirujuk != null ? alasandirujuk.AlasanDirujuk : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditRAD")]
        public string EditRAD_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region CHECK
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                var nobukti = SIMtrRJ.NoBukti;

                                var userid = User.Identity.GetUserId();
                                #endregion

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }
                                #endregion

                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.AnalisID = item.dokteranalis;
                                SIMtrRJ.Dirujuk = item.dirujuk;
                                SIMtrRJ.DirujukVendorID = item.dirujuk ? item.dirujukventor : "";
                                SIMtrRJ.TglKirim = item.dirujuk ? item.tanggaldirujukkirim : (DateTime?)null;
                                SIMtrRJ.TglTerima = item.dirujuk ? item.tanggaldirujukditerima : (DateTime?)null;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.AlasanDirujukID = item.dirujuk ? item.alasandirujuk : string.Empty;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.KdKelas = simtrregistrasi.KdKelas;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = false;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon
                                        };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;
                                    }
                                }
                                
                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                  
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== E D I T  H D
        [HttpGet]
        [ActionName("EditHD")]
        public ActionResult EditHD_Get(string nobukti, string sectionid)
        {
            Response.Cookies["SectionID"].Value = sectionid;
            Response.Cookies["SectionID"].Expires = DateTime.Now.AddDays(1);
            BillingInsertViewModel model;
            using (var s = new SIMEntities())
            {
                var m = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == nobukti);
                if (m == null) return HttpNotFound();
                var d = s.SIMtrRJTransaksi.Where(x => x.NoBukti == nobukti).ToList();
                var dd = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == nobukti).ToList();
                var dasistenanatesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == nobukti).ToList();
                var dinstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == nobukti).ToList();
                var donloop = s.SIMtrOperasiOnLoop.Where(x => x.NoBukti == nobukti).ToList();

                var dokter = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterID);
                model = new BillingInsertViewModel()
                {
                    nobukti = m.NoBukti,
                    jam = (m.Jam ?? new DateTime()).TimeOfDay,
                    nomor = m.Nomor ?? 0,
                    noreg = m.RegNo ?? m.NoReg,
                    section = m.SectionID,
                    tanggal = m.Tanggal ?? new DateTime(),
                    dokternama = dokter == null ? "" : dokter.NamaDOkter,
                    jumlah = 0,
                    detail = new List<BillingInsertDetailViewModel>(),
                    jenisluka = m.JenisLukaID,
                    dokterpengirim = m.DokterPengirimID,
                    dokter = m.OK_DokterBedahID,
                    opetatorpelaksasa = m.OK_DokterBedahID2,
                    durante = m.OK_DokterDuranteID,
                    dokterasisten = m.OK_DOkterAssBedah,
                    dokterasisten2 = m.OK_DOkterAssBedah2,
                    dokteranak = m.OK_DokterANak,
                    dokteranastesi = m.OK_DokterAnasID,
                    cito = m.Cyto ?? false,
                    spesialis = m.SpesialisasiID,
                    kelas = m.KdKelas,
                    audit = m.Audit ?? false,
                    adaobat = m.AdaObat ?? false,
                    asistenanastesi = new List<SelectListItemViewModel>(),
                    instrumen = new List<SelectListItemViewModel>(),
                    onloop = new List<SelectListItemViewModel>()
                };
                ViewBag.Noreg = m.RegNo;
                ViewBag.Section = m.SectionID;

                foreach (var x in dasistenanatesi)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterAsistenAnas);
                    model.asistenanastesi.Add(new SelectListItemViewModel() { Value = x.DokterAsistenAnas, Text = dok.NamaDOkter });
                }
                foreach (var x in dinstrumen)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterInstrumenID);
                    model.instrumen.Add(new SelectListItemViewModel() { Value = x.DokterInstrumenID, Text = dok.NamaDOkter });
                }
                foreach (var x in donloop)
                {
                    var dok = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterOnLoopID);
                    model.onloop.Add(new SelectListItemViewModel() { Value = x.DokterOnLoopID, Text = dok.NamaDOkter });
                }
                var sectionasal = s.SIMmSection.FirstOrDefault(x => x.SectionID == m.SectionAsalID);
                var kelas = s.SIMmKelas.FirstOrDefault(x => x.KelasID == m.KdKelas);
                var jenisanatesi = s.mJenisAnestesi.FirstOrDefault(x => x.IDAnastesi == m.OK_JenisAnastesi);
                model.jenisanatesi = jenisanatesi.IDAnastesi;
                var spesialisasi = s.SIMmSpesialisasi.FirstOrDefault(x => x.SpesialisID == m.SpesialisasiID);
                var ruangok = s.SIMmRuangOK.FirstOrDefault(x => x.RuangOKID == m.RuangOK);
                ViewBag.PenanggungNama = m.OK_PenanggungNama;
                ViewBag.PenanggungAlamat = m.OK_PenanggungAlamat;
                ViewBag.PenanggungHubungan = m.OK_PenanggungHub;
                ViewBag.SectionAsalName = sectionasal == null ? "" : sectionasal.SectionName;
                ViewBag.KelasName = kelas == null ? "" : kelas.NamaKelas;
                ViewBag.JenisAnatesiName = jenisanatesi == null ? "" : jenisanatesi.Deskripsi;
                ViewBag.RuangOKName = ruangok == null ? "" : ruangok.RuangOK;
                var kategorioperasi = s.SIMmKategoriOperasi.FirstOrDefault(x => x.KategoriID == m.OK_KategoriOperasiID);
                ViewBag.KategoriOperasiName = kategorioperasi == null ? "" : kategorioperasi.KategoriName;
                var jenisluka = s.SIMmJenisLuka.FirstOrDefault(x => x.KodeLuka == m.JenisLukaID);
                ViewBag.JenisLukaName = jenisluka == null ? "" : jenisluka.Deskripsi;
                var dokterpengirim = s.mDokter.FirstOrDefault(x => x.DokterID == m.DokterPengirimID);
                ViewBag.DokterPengirimName = dokterpengirim == null ? "" : dokterpengirim.NamaDOkter;

                var detailid = 0;
                foreach (var x in d)
                {
                    dokter = s.mDokter.FirstOrDefault(y => y.DokterID == x.DokterID);
                    var jasa = s.SIMmListJasa.FirstOrDefault(y => y.JasaID == x.JasaID);
                    var detail = new BillingInsertDetailViewModel()
                    {
                        diskon = (decimal)(x.Disc),
                        dokter = x.DokterID,
                        namadokter = dokter == null ? "" : dokter.NamaDOkter,
                        jasaid = x.JasaID,
                        jasanama = jasa.JasaName,
                        noreg = model.noreg,
                        qty = (decimal)(x.Qty ?? 0),
                        section = model.section,
                        tarif = x.Tarif,
                        id = detailid,
                        detail = new List<BillingInsertDetailJasaViewModel>()
                    };
                    detailid++;
                    model.jumlah += detail.qty * (detail.tarif - detail.diskon);
                    foreach (var y in dd.Where(z => z.Nomor == x.Nomor).ToList())
                    {
                        var komponen = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.KomponenID);
                        var diskonnilai = (y.Harga / 100) * (decimal)y.Disc;
                        detail.detail.Add(new BillingInsertDetailJasaViewModel()
                        {
                            diskon = (decimal)y.Disc,
                            harga = y.Harga,
                            komponenid = y.KomponenID,
                            diskonnilai = diskonnilai,
                            komponennama = komponen.KomponenName,
                            jumlah = y.Harga = diskonnilai
                        });
                    }
                    model.detail.Add(detail);
                }

                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokter);
                ViewBag.Operator = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.opetatorpelaksasa);
                ViewBag.OperatorPelaksana = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.durante);
                ViewBag.Durante = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten);
                ViewBag.DokterAsisten = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokterasisten2);
                ViewBag.DokterAsisten2 = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranak);
                ViewBag.DokterAnak = dokter != null ? dokter.NamaDOkter : "";
                dokter = s.mDokter.FirstOrDefault(x => x.DokterID == model.dokteranastesi);
                ViewBag.Anastesi = dokter != null ? dokter.NamaDOkter : "";
            }
            if (Request.IsAjaxRequest())
                return PartialView(model);
            else
                return View(model);
        }

        [HttpPost]
        [ActionName("EditHD")]
        public string EditHD_Post()
        {
            try
            {
                var item = new BillingInsertViewModel();
                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    using (var s = new SIMEntities())
                    {
                        using (var dbContextTransaction = s.Database.BeginTransaction())
                        {
                            try
                            {
                                #region CHECK
                                var simtrregistrasi = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == item.noreg);
                                if (simtrregistrasi.ProsesPayment == true) throw new Exception("Data Sedang di proses di kasir");

                                var SIMtrRJ = s.SIMtrRJ.FirstOrDefault(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJ.Audit ?? false) throw new Exception("Tidak dapat diubah karena sudah di audit");
                                var nobukti = SIMtrRJ.NoBukti;

                                var userid = User.Identity.GetUserId();
                                #endregion

                                #region ==== REMOVE DETAIL
                                var SIMtrRJTransaksiDetail = s.SIMtrRJTransaksiDetail.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksiDetail != null)
                                {
                                    s.SIMtrRJTransaksiDetail.RemoveRange(SIMtrRJTransaksiDetail);
                                }

                                var SIMtrRJTransaksi = s.SIMtrRJTransaksi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrRJTransaksi != null)
                                {
                                    s.SIMtrRJTransaksi.RemoveRange(SIMtrRJTransaksi);
                                }

                                var SIMtrOperasiAnastesi = s.SIMtrOperasiAnastesi.Where(x => x.NoBukti == item.nobukti);
                                if (SIMtrOperasiAnastesi != null)
                                {
                                    s.SIMtrOperasiAnastesi.RemoveRange(SIMtrOperasiAnastesi);
                                }
                                var SIMtrOperasiInstrumen = s.SIMtrOperasiInstrumen.Where(x => x.NoBUkti == item.nobukti);
                                if (SIMtrOperasiInstrumen != null)
                                {
                                    s.SIMtrOperasiInstrumen.RemoveRange(SIMtrOperasiInstrumen);
                                }
                                #endregion

                                SIMtrRJ.KdKelas = item.kelas;
                                SIMtrRJ.DokterPengirimID = item.dokterpengirim;
                                SIMtrRJ.OK_JenisAnastesi = item.jenisanatesi;
                                SIMtrRJ.Cyto = item.cito;
                                SIMtrRJ.OK_DokterBedahID = item.dokter;
                                SIMtrRJ.DokterID = item.dokter;
                                SIMtrRJ.JenisKerjasamaID = simtrregistrasi.JenisKerjasamaID;
                                SIMtrRJ.Tanggal = item.tanggal;
                                SIMtrRJ.Jam = item.tanggal + item.jam;
                                SIMtrRJ.SectionID = item.section;
                                SIMtrRJ.UserID = 1103;
                                SIMtrRJ.NRM = simtrregistrasi.NRM;
                                SIMtrRJ.Status = "O";
                                SIMtrRJ.Batal = false;
                                SIMtrRJ.AdaObat = item.adaobat;
                                SIMtrRJ.Audit = item.audit;
                                SIMtrRJ.UserIDWeb = userid;
                                SIMtrRJ.MCU = false;

                                var r = s.SaveChanges();

                                var nomorbukti = 1;
                                if (item.detail == null) item.detail = new List<BillingInsertDetailViewModel>();
                                if (item.detail.Count != 0)
                                {
                                    foreach (var x in item.detail)
                                    {
                                        var i = new SIMtrRJTransaksi()
                                        {
                                            NoBukti = nobukti,
                                            Nomor = nomorbukti,
                                            JasaID = x.jasaid,
                                            KelasID = simtrregistrasi.KdKelas,
                                            Qty = (double)x.qty,
                                            Tarif = x.tarif,
                                            Jam = item.tanggal + item.jam,
                                            UserID = 1103,
                                            NRM = simtrregistrasi.NRM,
                                            Waktu = item.tanggal + item.jam,
                                            PasienKTP = simtrregistrasi.PasienKTP,
                                            DokterID = x.dokter,
                                            Disc = (double)x.diskon
                                        };
                                        s.SIMtrRJTransaksi.Add(i);
                                        foreach (var y in x.detail)
                                        {
                                            var komponenbiaya = s.SIMmKomponenBiaya.FirstOrDefault(z => z.KomponenBiayaID == y.komponenid);
                                            var j = new SIMtrRJTransaksiDetail()
                                            {
                                                NoBukti = nobukti,
                                                Nomor = nomorbukti,
                                                JasaID = x.jasaid,
                                                KomponenID = y.komponenid,
                                                Harga = y.harga,
                                                KelompokAkun = komponenbiaya.KelompokAkun,
                                                PostinganKe = "GL",
                                                HargaOrig = y.harga,
                                                Disc = (double)y.diskon
                                            };
                                            s.SIMtrRJTransaksiDetail.Add(j);
                                        }
                                        nomorbukti++;
                                    }
                                }

                                if (item.asistenanastesi == null) item.asistenanastesi = new List<SelectListItemViewModel>();
                                foreach (var x in item.asistenanastesi)
                                {
                                    s.SIMtrOperasiAnastesi.Add(new SIMtrOperasiAnastesi() { NoBukti = nobukti, DokterAsistenAnas = x.Value });
                                }
                                if (item.instrumen == null) item.instrumen = new List<SelectListItemViewModel>();
                                foreach (var x in item.instrumen)
                                {
                                    s.SIMtrOperasiInstrumen.Add(new SIMtrOperasiInstrumen() { NoBUkti = nobukti, DokterInstrumenID = x.Value });
                                }

                                simtrregistrasi.StatusPeriksa = "Sudah Periksa";
                                var simtrdataregpasien = s.SIMtrDataRegPasien.FirstOrDefault(x => x.NoReg == simtrregistrasi.NoReg && x.Nomor == item.nomor && x.SectionID == item.section);
                                
                                r = s.SaveChanges();
                                var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                                {
                                    Activity = $"Billing Create {nobukti}"
                                };
                                UserActivity.InsertUserActivity(userActivity);
                                dbContextTransaction.Commit();
                                return new JavaScriptSerializer().Serialize(new
                                {
                                    IsSuccess = true
                                });
                            }
                            catch (DbEntityValidationException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(StaticModel.DbEntityValidationExceptionToString(ex));
                            }
                            catch (SqlException ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                            catch (Exception ex)
                            {
                                dbContextTransaction.Rollback();
                                throw new Exception(ex.Message);
                            }
                        }
                    }
                }
                else
                    return JsonHelper.JsonMsgError(ViewData);
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== L I S T  J A S A
        [HttpPost]
        public string ListJasa(string sortBy, IEnum.Sorting sortByType, int pageSize, int pageIndex, string[] filter)
        {
            try
            {
                ResultSS result;
                using (var s = new SIMEntities())
                {
                    IQueryable<Pelayanan_LookupTarif_Result> proses = s.Pelayanan_LookupTarif(filter[10] == "True" ? "" : Request.Cookies["SectionID"].Value);
                    if (!string.IsNullOrEmpty(filter[0]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaID)}.Contains(@0)", filter[0]);
                    if (!string.IsNullOrEmpty(filter[1]))
                        proses = proses.Where($"{nameof(Pelayanan_LookupTarif_Result.JasaName)}.Contains(@0)", filter[1]);
                    var totalcount = proses.Count();
                    var models = proses.OrderBy($"{sortBy} {(sortByType == IEnum.Sorting.ASC ? "ASC" : "DESC")}")
                        .Skip((pageIndex) * pageSize).Take(pageSize).ToArray();
                    result = new ResultSS(models.Length, models, totalcount, pageIndex);
                    var m = models.ToList().ConvertAll(x => IConverter.Cast<Pelayanan_LookupTarif_Result>(x));
                    result.Data = m;
                }
                return JsonConvert.SerializeObject(new TableList(result));
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        #region ==== G E T  T A R I F
        [HttpPost]
        public string GetTarif(string jasa, string dokter, string section, string noreg, int? kategorioperasi, int? cito)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var p = s.SIMtrRegistrasi.FirstOrDefault(x => x.NoReg == noreg);
                    var c = s.SIMmSection.FirstOrDefault(x => x.SectionID == section);
                    var h = s.GetTarifBiaya_Global(jasa, dokter, cito, kategorioperasi, section, noreg, "1").FirstOrDefault();
                    if (h == null) throw new Exception("GetTarifBiaya_Global tidak ditemukan");
                    var d = s.GetDetailKomponenTarif_Global(h.ListHargaID, h.CustomerKerjasamaID, h.TarifBaru, p.JenisKerjasamaID, "1", c.UnitBisnisID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Detail = d,
                        Tarif = h.Harga ?? 0
                    });
                }
            }
            catch (SqlException ex) { return JsonHelper.JsonMsgError(ex); }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }

        #endregion
    }
}
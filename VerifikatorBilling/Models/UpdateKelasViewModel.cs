﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifikatorBilling.Models
{
    public class UpdateKelasViewModel
    {
        public string NoReg { get; set; }
        public int Nomor { get; set; }
        public string KelasAsalID { get; set; }
        public string KelasPelayananID { get; set; }
        public bool TitipKelas { get; set; }

        public string Reg_KdKelasPertanggungan { get; set; }
        public string Reg_KdKelasAsal { get; set; }
        public string Reg_KdKelas { get; set; }
    }
}
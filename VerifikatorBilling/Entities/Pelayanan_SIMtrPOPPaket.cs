//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VerifikatorBilling.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Pelayanan_SIMtrPOPPaket
    {
        public string NamaPaket { get; set; }
        public string NamaDOkter { get; set; }
        public string NoBuktiPOP { get; set; }
        public System.DateTime Tanggal { get; set; }
        public System.DateTime Jam { get; set; }
        public string SectionID { get; set; }
        public decimal JumlahTransaksi { get; set; }
        public string NoBuktiParent { get; set; }
        public Nullable<short> User_ID { get; set; }
        public bool Paket { get; set; }
        public string PaketObat { get; set; }
        public string DokterID { get; set; }
        public decimal KomisiDokter { get; set; }
        public string NRM { get; set; }
        public string KelasID { get; set; }
        public Nullable<bool> KTP { get; set; }
        public string NoReg { get; set; }
        public string KerjasamaID { get; set; }
        public string PerusahaanID { get; set; }
        public bool RawatInap { get; set; }
        public bool Audit { get; set; }
        public double PPN { get; set; }
        public bool Batal { get; set; }
        public string KategoriPlafon { get; set; }
        public string NamaPlafon { get; set; }
        public Nullable<bool> Ditagihkan { get; set; }
        public Nullable<bool> ProsesAudit { get; set; }
        public string SectionInput { get; set; }
        public string UnitBisnisID { get; set; }
        public Nullable<decimal> CustomerKerjasamaID { get; set; }
        public Nullable<bool> ObatTrolley { get; set; }
    }
}

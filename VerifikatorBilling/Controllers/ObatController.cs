﻿using iHos.MVC.Converter;
using iHos.MVC.Property;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VerifikatorBilling.Entities;
using VerifikatorBilling.Models;

namespace VerifikatorBilling.Controllers
{
    [Authorize(Roles = "VerifikatorBilling")]
    public class ObatController : Controller
    {
        #region ==== I N D E X
        public ActionResult Index()
        {
            return View();
        }
        #endregion

        #region ==== G E T  D A T A  O B A T
        [HttpPost]
        public string GetDataObat(string noreg, string status)
        {
            try
            {
                var item = new VerifikasiViewModel();
                using (var s = new SIMEntities())
                {
                    var detail = s.Verif_DataObat(noreg).ToList();
                    if (!string.IsNullOrEmpty(status))
                    {
                        var audit = status == "y" ? true : false;
                        detail = detail.Where(x => x.NoReg == noreg && x.Audit == audit).ToList();
                    }

                    item.detail = new List<VerifikasiDetailViewModel>();
                    foreach (var x in detail)
                    {
                        var y = new VerifikasiDetailViewModel()
                        {
                            NilaiPendapatan = x.Nilai == null ? 0 :(decimal)x.Nilai,
                            NilaiPendapatan_View = x.Nilai == null ? "0" : x.Nilai.Value.ToMoney(),
                            NoBukti = x.NoBukti,
                            NoReg = x.NoReg,
                            SectionID = x.SectionID,
                            DokterID = x.DokterID,
                            NamaDOkter = x.NamaDokter,
                            SectionName = x.SectionName,
                            Tanggal = x.Tanggal,
                            Tanggal_View = x.Tanggal.Value.ToString("dd/MM/yyyy"),
                            Transaksi_Dikunci = x.Audit.Value
                        };
                        item.detail.Add(y);
                    }
                }
                var result = new
                {
                    IsSuccess = true,
                    Data = item
                };
                return JsonConvert.SerializeObject(result);
            }
            catch (Exception ex) { return JsonHelper.JsonMsgError(ex); }
        }
        #endregion

        [HttpGet]
        [ActionName("Detail")]
        public ActionResult Detail(string nobukti)
        {
            ResepViewModel item;
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.BILLFarmasi.FirstOrDefault(x => x.NoBukti == nobukti);
                    if (m == null) return HttpNotFound();
                    item = IConverter.Cast<ResepViewModel>(m);
                    item.KodePaket = m.KodePaket;

                    var dokter = s.mDokter.Where(x => x.DokterID == m.DokterID).FirstOrDefault();
                    item.Dokter = (dokter != null)? dokter.NamaDOkter : "";

                    var section = s.SIMmSection.Where(x => x.SectionID == m.SectionID).FirstOrDefault();
                    item.NamaFarmasi = (section != null) ? section.SectionName : "";

                    var section2 = s.SIMmSection.Where(x => x.SectionID == m.SectionAsalID).FirstOrDefault();
                    item.SectionName = (section2 != null) ? section2.SectionName : "";


                    item.Jam_View = m.Jam.ToString("HH:mm");
                    item.Tanggal_View = m.Tanggal.ToString("dd/MM/yyyy");
                    item.Jumlah_View = m.Total.Value.ToMoney();
                    var d = s.BILLFarmasiDetail.OrderBy(x => x.Nama_Barang).Where(x => x.NoBukti == nobukti).ToList();
                    item.Detail_List = new ListDetail<ResepDetailViewModel>();
                    foreach (var x in d)
                    {
                        var n = IConverter.Cast<ResepDetailViewModel>(x);

                        var barang = s.mBarang.Where(xx => xx.Barang_ID == x.Barang_ID).FirstOrDefault();
                        n.Kode_Barang = (barang != null) ? barang.Kode_Barang : "";
                        n.Nama_Barang = (barang != null) ? barang.Nama_Barang : "";
                        n.AturanPakai = x.Dosis;
                        n.Qty = (double)(x.JmlObat - x.JmlRetur);

                        var jumlah = (decimal)(x.JmlObat - x.JmlRetur) * x.Harga;
                        n.Jumlah_View = jumlah.ToMoney();
                        n.Harga_View = x.Harga.ToMoney();
                        item.Detail_List.Add(false, n);
                    }
                }
            }
            catch (SqlException ex) { throw new Exception(ex.Message); }
            catch (Exception ex) { throw new Exception(ex.Message); }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            if (Request.IsAjaxRequest())
                return PartialView(item);
            else
                return View(item);
        }
    }
}